extends KinematicBody2D

var velocity = Vector2.ZERO
var gravity
var flap = 12
var color = ["Red", "Green", "Yellow", "Blue"]

func _ready():
	var sprite = get_node(color[randi() % 4])
	sprite.playing = true
	sprite.visible = true
	gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func _physics_process(delta):
	if Input.is_action_just_pressed("flap"):
		velocity.y = -flap
	else:
		velocity.y += gravity * delta
	var collision = move_and_collide(velocity)
	if collision:
		velocity = velocity.bounce(collision.normal) * 0.6
