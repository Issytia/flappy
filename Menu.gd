extends PopupPanel

func _process(delta):
	if get_tree().paused and Input.is_action_just_pressed("pause"):
		get_parent().get_parent()._process(delta)
