extends Node2D

var playing = false
var player
var ground = [null, null]

func _ready():
	randomize()
	$AnimationPlayer.play("Background")
	$MenuLayer/Menu/CC/VB/Resume.connect("pressed", self, "resume")
	$MenuLayer/Menu/CC/VB/New.connect("pressed", self, "start")
	$MenuLayer/Menu/CC/VB/Quit.connect("pressed", self, "quit")
	open_menu()

func start():
	if playing:
		return

	close_menu()

	# add ground
	ground[0] = preload("res://Ground.tscn").instance()
	ground[0].position = Vector2(-400, 200)
	add_child(ground[0])
	ground[1] = preload("res://Ground.tscn").instance()
	ground[1].position = Vector2(400, 200)
	add_child(ground[1])

	# add player
	player = preload("res://Player.tscn").instance()
	player.position = Vector2(-300, 0)
	add_child(player)

	playing = true

func lose():
	if not playing:
		return

	playing = false
	remove_child(player)
	remove_child(ground[0])
	ground[0] = null
	remove_child(ground[1])
	ground[1] = null
	open_menu()

func pause():
	get_tree().paused = true
	open_menu()

func resume():
	close_menu()
	get_tree().paused = false

func open_menu():
	if get_tree().paused:
		$MenuLayer/Menu/CC/VB/Resume.visible = true
		$MenuLayer/Menu/CC/VB/New.visible = false
	else:
		$MenuLayer/Menu/CC/VB/Resume.visible = false
		$MenuLayer/Menu/CC/VB/New.visible = true
	$MenuLayer/Menu.visible = true

func close_menu():
	$MenuLayer/Menu.visible = false

func quit():
	get_tree().quit()

func _process(delta):
	if not playing:
		return

	if Input.is_action_just_pressed("pause"):
		Input.action_release("pause")
		if get_tree().paused:
			resume()
		else:
			pause()

func _physics_process(delta):
	if not playing:
		return

	ground[0].position.x -= 75 * delta
	ground[1].position.x -= 75 * delta
	if ground[0].position.x < -800:
		var temp = ground[0]
		ground[0] = ground[1]
		ground[1] = temp
		ground[1].position.x = ground[0].position.x + 800

	var threshold = 40
	if player.position.y > (240 + threshold) or player.position.y < -(240 + threshold) or player.position.x < -(400 + threshold):
		lose()
